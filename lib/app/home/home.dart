import 'package:flutter/material.dart';

import '../../main.dart';
import 'home-item.dart';

class Home extends StatelessWidget {
  Map<String, HomeItem> items = {
    VocabConfigRoute: HomeItem(
        Color.fromRGBO(255, 255, 255, 0.2), "Vokabeltraining", "images/1.jpg"),
    GrammarRoute: HomeItem(Color.fromRGBO(255, 255, 255, 0.2),
        "Regeln der Grammatik", "images/2.jpg"),
    ExampleRoute: HomeItem(
        Color.fromRGBO(255, 255, 255, 0.2), "Beispielsätze", "images/3.jpg"),
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Übung wählen:'),
        ),
        body: SingleChildScrollView(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: (items.entries.map((entry) => GestureDetector(
              child: entry.value,
              onTap: () => Navigator.pushNamed(context, entry.key)))).toList(),
        )));
  }
}
