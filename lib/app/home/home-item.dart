import 'dart:ui';

import 'package:flutter/material.dart';

class HomeItem extends StatelessWidget {
  final String name;
  final String imageURL;
  final Color color;
  final double _blur = 16.0;

  HomeItem(this.color, this.name, this.imageURL);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
      alignment: Alignment.bottomLeft,
      height: 200.0,
      decoration: new BoxDecoration(
        image: new DecorationImage(
          image: new AssetImage(this.imageURL),
          fit: BoxFit.cover,
        ),
      ),
      child: ClipRect(
        child: Container(
          width: double.infinity,
          height: 70.0,
          color: this.color,
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: this._blur, sigmaY: this._blur),
            child: Container(
              padding: EdgeInsets.fromLTRB(20, 15, 0, 0),
              child: Text(
                this.name,
                style: TextStyle(fontSize: 30, color: Color.fromRGBO(255, 255, 255, 1)),
              ),
            ),
          ),
        ),
      ),
    );
    //;
  }
}
