import 'package:flutter/material.dart';
import 'package:vocab/app/vocab/vocab-display-item.dart';
import 'package:vocab/model/vocab-data.dart';

class Vocab extends StatelessWidget {
  int numberOfItems = 20;
  int topN = 100;
  bool direction = true;
  VocabData vocab;

  Vocab(this.vocab);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<VocabDataItem>>(
      future: vocab.load(), // function where you call your api
      builder:
          (BuildContext context, AsyncSnapshot<List<VocabDataItem>> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return _getWaitingWidget(context);
        } else {
          if (snapshot.hasError)
            return _getErrorWidget(context);
          else
            return _getVocabListWidget(snapshot.data);
        }
      },
    );
  }

  Widget _getWaitingWidget(context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Vokabeltraining'),
      ),
      body: Center(
        child:
            Text("Lädt Daten...", style: Theme.of(context).textTheme.bodyText1),
      ),
    );
  }

  Widget _getErrorWidget(context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Vokabeltraining'),
      ),
      body: Center(
        child: Text("Fehler! Konnte keine Daten laden.",
            style: Theme.of(context).textTheme.bodyText1),
      ),
    );
  }

  Widget _getVocabListWidget(List<VocabDataItem> data) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Vokabeltraining'),
      ),
      body: ListView.builder(
          itemCount: data.length,
          itemBuilder: (BuildContext ctxt, int index) {
            return VocabDisplayItem(data[index]);
          }),
    );
  }
}
