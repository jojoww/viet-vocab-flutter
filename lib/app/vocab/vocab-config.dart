import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vocab/app/basic-elements/adaptive-button.dart';
import 'package:vocab/app/basic-elements/adaptive-selection.dart';
import 'package:vocab/model/vocab-data.dart';

import '../../main.dart';

class VocabConfig extends StatefulWidget {
  @override
  _VocabConfigState createState() => _VocabConfigState();
}

class _VocabConfigState extends State<VocabConfig> {
  final Map<int, Text> topN = {
    10: Text("10"),
    20: Text("20"),
    50: Text("50"),
    100: Text("100"),
    500: Text("500"),
    99999: Text("Alle"),
  };
  final Map<int, Text> numberOfItems = {
    10: Text("10"),
    50: Text("50"),
    100: Text("100")
  };

  final Map<int, Text> direction = {
    0: Text("Deutsch-Viet"),
    1: Text("Viet-Deutsch"),
  };

  int selectedTopN = 100;
  int selectedNumberOfItems = 100;
  int selectedDirection = 0;
  final EdgeInsets insets = EdgeInsets.fromLTRB(40, 40, 20, 10 );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Einstellungen'),
        ),
        body: SingleChildScrollView(
          child: Column(children: [
          Padding(
            padding: insets,
            child: Text(
                "Aus welchem Bereich sollen Vokabeln verwendet werden? Die Top 10 sind bspw. die 10 wichtigsten Wörter.",
                style: Theme.of(context).textTheme.bodyText1),
          ),
          AdaptiveSelection(
            topN,
            (int val) {
              setState(() {
                selectedTopN = val;
              });
            },
            selectedTopN,
          ),
          Padding(
            padding: insets,
            child: Text("Wie viele Wörter sollen geladen werden?",
                style: Theme.of(context).textTheme.bodyText1),
          ),
          AdaptiveSelection(
            numberOfItems,
            (int val) {
              setState(() {
                selectedNumberOfItems = val;
              });
            },
            selectedNumberOfItems,
          ),
          Padding(
            padding: insets,
            child: Text("In welche Richtung soll übersetzt werden?",
                style: Theme.of(context).textTheme.bodyText1),
          ),
          AdaptiveSelection(
            direction,
            (int val) {
              setState(() {
                selectedDirection = val;
              });
            },
            selectedDirection,
          ),
          Padding(
            padding: insets,
            child: AdaptiveButton("Los geht's", startVocab),
          ),
        ])));
  }

  startVocab() {
    print("Let's go");
    Navigator.pushNamed(context, VocabRoute, arguments: {
      "vocab": VocabData(this.selectedNumberOfItems, this.selectedTopN, this.selectedDirection == 0)
    });
  }
}
