import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vocab/model/vocab-data.dart';

class VocabDisplayItem extends StatefulWidget {
  final VocabDataItem item;

  VocabDisplayItem(this.item);

  @override
  _VocabDisplayItemState createState() => _VocabDisplayItemState(this.item);
}

class _VocabDisplayItemState extends State<VocabDisplayItem>
    with AutomaticKeepAliveClientMixin {
  bool _visible = false;
  final VocabDataItem _item;

  _VocabDisplayItemState(this._item);

  makeVisible() {
    print("Clicked!");
    setState(() {
      _visible = true;
    });
  }

  @override
  bool get wantKeepAlive => true;
  
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: Container(
                height: 100.0,
                child: Card(
                  elevation: 8,
                  color: Colors.blueGrey[800],
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: FittedBox(
                        child: Text(_item.language1,
                            style: Theme.of(context)
                                .textTheme
                                .headline2
                                .copyWith(fontSize: 40.0)),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            AnimatedOpacity(
              // If the widget is visible, animate to 0.0 (invisible).
              // If the widget is hidden, animate to 1.0 (fully visible).
              opacity: _visible ? 1.0 : 0.0,
              duration: Duration(milliseconds: 500),
              // The green box must be a child of the AnimatedOpacity widget.
              child: Padding(
                padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
                child: Container(
                  child: Card(
                    elevation: 8,
                    color: Colors.blueGrey[100],
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                        child: FittedBox(
                          child: Column(children: [
                            Text(_item.language2,
                                style: Theme.of(context)
                                    .textTheme
                                    .headline3
                                    .copyWith(fontSize: 40.0)),
                            Text(_item.description,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    .copyWith(fontSize: 40.0))
                          ]),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        onTap: () => {makeVisible()});
  }
}
