import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../main.dart';

class AdaptiveButton extends StatelessWidget {
  final String text;
  final Function onPressed;

  AdaptiveButton(this.text, this.onPressed);

  @override
  Widget build(BuildContext context) {
    return ios
        ? CupertinoButton.filled(
            child: Text(this.text),
            onPressed: this.onPressed,
          )
        : ElevatedButton(onPressed: this.onPressed, child: Text(this.text));
  }
}
