import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AdaptiveSelection extends StatelessWidget {
  final Map<int, Text> values;
  final Function onValueChanged;
  final int startValue;

  AdaptiveSelection(this.values, this.onValueChanged, this.startValue);

  @override
  Widget build(BuildContext context) {
    return CupertinoSlidingSegmentedControl<int>(
      children: this.values,
      onValueChanged: this.onValueChanged,
      groupValue: this.startValue,
    );
  }
}
