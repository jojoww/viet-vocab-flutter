import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vocab/app/vocab/vocab-config.dart';
import 'package:vocab/app/vocab/vocab.dart';

import 'app/home/home.dart';

void main() {
  runApp(MyApp());
}

const HomeRoute = '/';
const VocabConfigRoute = '/vocab_config';
const VocabRoute = '/vocab';
const GrammarRoute = '/grammar';
const ExampleRoute = '/examples';
const ios = true;

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      onGenerateRoute: _routes(),
      theme: _themeAndroid(),
      builder: (context, widget) {
        return ScrollConfiguration(
            behavior: ScrollBehaviorModified(), child: widget);
      },
    );
  }

  ThemeData _themeAndroid() {
    return ThemeData(
        // Define the default brightness and colors.
        primaryColor: Colors.grey[900],
        accentColor: Colors.cyan[600],

        // Define the default font family.
        fontFamily: 'Calistoga',

        // Define the default TextTheme. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: TextTheme(
          headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          headline2: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold, color: Colors.white),
          headline3: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
          headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
          bodyText1: TextStyle(fontSize: 18.0, fontFamily: 'Calistoga'),
          bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Calistoga'),
        ),
        pageTransitionsTheme: PageTransitionsTheme(builders: {
          TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
          TargetPlatform.android: CupertinoPageTransitionsBuilder(),
        }),
      );
  }

  RouteFactory _routes() {
    return (settings) {
      final Map<String, dynamic> arguments = settings.arguments;
      Widget screen;
      switch (settings.name) {
        case HomeRoute:
          screen = Home();
          break;
        case VocabConfigRoute:
          screen = VocabConfig();
          break;
        case VocabRoute:
          screen = Vocab(arguments['vocab']);
          break;
        // case VocabConfigRoute:
        //   screen = LocationDetail(arguments['id']);
        //   break;
        default:
          return null;
      }
      return MaterialPageRoute(builder: (BuildContext context) => screen);
    };
  }
}

class ScrollBehaviorModified extends ScrollBehavior {
  const ScrollBehaviorModified();
  @override
  ScrollPhysics getScrollPhysics(BuildContext context) {
    switch (getPlatform(context)) {
      case TargetPlatform.iOS:
      case TargetPlatform.macOS:
      case TargetPlatform.android:
        return const BouncingScrollPhysics();
      case TargetPlatform.fuchsia:
      case TargetPlatform.linux:
      case TargetPlatform.windows:
        return const ClampingScrollPhysics();
    }
    return null;
  }
}