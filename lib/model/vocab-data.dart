import 'dart:convert';
import 'dart:math';

import 'package:flutter/services.dart';

class VocabData {
  int numberOfItems;
  int topN;
  bool direction;

  VocabData(this.numberOfItems, this.topN, this.direction);

  Future<List<VocabDataItem>> load() async {
    List<VocabDataItem> result = [];
    String text = await rootBundle.loadString("assets/vocab-data.json");
    final jsonData = json.decode(text);
    jsonData["data"]["vnde"].forEach((item) => {
          result.add(VocabDataItem(
              this.direction ? item["language1"] : item["language2"],
              this.direction ? item["language2"] : item["language1"],
              item["description"]))
        });
    var resultTopN = result.sublist(0, min(this.topN, result.length)).toList();
    resultTopN.shuffle();
    return Future.value(
        resultTopN.sublist(0, min(this.numberOfItems, resultTopN.length)));
  }
}

class VocabDataItem {
  String language1;
  String language2;
  String description;
  VocabDataItem(this.language1, this.language2, this.description);
}
